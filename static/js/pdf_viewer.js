odoo.define('pdf_viewer.pdf_viewer', function (require) {
    var PublicWidget = require('web.public.widget');
    var rpc = require('web.rpc');
    var PdfViewer = PublicWidget.Widget.extend({
        selector: '#pdf-main-container',
        start: function () {
            const container = this.el;
            
            var _PDF_DOC,
                _CURRENT_PAGE,
                _TOTAL_PAGES,
                _PAGE_RENDERING_IN_PROGRESS = 0,
                _CANVAS = container.querySelector('#pdf-canvas');

            _CANVAS.width = container.offsetWidth

            // initialize and load the PDF
            async function showPDF(pdf_url) {
                container.querySelector("#pdf-loader").style.display = 'block';

                // get handle of pdf document
                try {
                    _PDF_DOC = await pdfjsLib.getDocument({ url: pdf_url });
                }
                catch(error) {
                    alert(error.message);
                }

                // total pages in pdf
                _TOTAL_PAGES = _PDF_DOC.numPages;
                
                // Hide the pdf loader and show pdf container
                container.querySelector("#pdf-loader").style.display = 'none';
                container.querySelector("#pdf-contents").style.display = 'block';
                container.querySelector("#pdf-total-pages").innerHTML = _TOTAL_PAGES;

                // show the first page
                showPage(1);
            }

            // load and render specific page of the PDF
            async function showPage(page_no) {
                _PAGE_RENDERING_IN_PROGRESS = 1;
                _CURRENT_PAGE = page_no;

                // disable Previous & Next buttons while page is being loaded
                container.querySelector("#pdf-next").disabled = true;
                container.querySelector("#pdf-prev").disabled = true;

                // while page is being rendered hide the canvas and show a loading message
                container.querySelector("#pdf-canvas").style.display = 'none';
                container.querySelector("#page-loader").style.display = 'block';

                // update current page
                container.querySelector("#pdf-current-page").innerHTML = page_no;
                
                // get handle of page
                try {
                    var page = await _PDF_DOC.getPage(page_no);
                }
                catch(error) {
                    console.log(error.message);
                }

                // original width of the pdf page at scale 1
                var pdf_original_width = page.getViewport(1).width;
                
                // as the canvas is of a fixed width we need to adjust the scale of the viewport where page is rendered
                var scale_required = _CANVAS.width / pdf_original_width;

                // get viewport to render the page at required scale
                var viewport = page.getViewport(scale_required);

                // set canvas height same as viewport height
                _CANVAS.height = viewport.height;

                // setting page loader height for smooth experience
                container.querySelector("#page-loader").style.height =  _CANVAS.height + 'px';
                container.querySelector("#page-loader").style.lineHeight = _CANVAS.height + 'px';

                var render_context = {
                    canvasContext: _CANVAS.getContext('2d'),
                    viewport: viewport
                };
                    
                // render the page contents in the canvas
                try {
                    await page.render(render_context);
                }
                catch(error) {
                    alert(error.message);
                }

                _PAGE_RENDERING_IN_PROGRESS = 0;

                // re-enable Previous & Next buttons
                container.querySelector("#pdf-next").disabled = false;
                container.querySelector("#pdf-prev").disabled = false;

                // show the canvas and hide the page loader
                container.querySelector("#pdf-canvas").style.display = 'block';
                container.querySelector("#page-loader").style.display = 'none';
            }

            // // click on "Show PDF" buuton
            // container.querySelector("#show-pdf-button").addEventListener('click', function() {
            //     this.style.display = 'none';
            //     showPDF('https://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf');
            // });

            // click on the "Previous" page button
            container.querySelector("#pdf-prev").addEventListener('click', function() {
                if(_CURRENT_PAGE != 1)
                    showPage(--_CURRENT_PAGE);
            });

            // click on the "Next" page button
            container.querySelector("#pdf-next").addEventListener('click', function() {
                if(_CURRENT_PAGE != _TOTAL_PAGES)
                    showPage(++_CURRENT_PAGE);
            });

            let data = container.getAttribute('data')
            if(data){
                let base64 = data.replace("b'", '').replace("'", '')
                let bytes = window.atob(base64)
                let length = bytes.length;
                let out = new Uint8Array(length);

                while (length--) {
                    out[length] = bytes.charCodeAt(length);
                }

                const blob = new Blob([out], { type: 'application/pdf' });
                const url = URL.createObjectURL(blob);
                showPDF(url);
            }
        },
    });
    PublicWidget.registry.pdf_viewer = PdfViewer;
    return PdfViewer;
 });