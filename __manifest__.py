{
    'name': "Средство просмотра PDF",
    'version': '1.0',
    'depends': ['base', 'website'],
    'author': "Xorikita",
    'category': 'Category',
    'description': """
    Description text
    """,
    # data files always loaded at installation
    'data': [
        'views/templates.xml',
    ],

    'assets': {
        'web.assets_frontend': [
            'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.2.228/pdf.min.js',
            '/pdf_viewer/static/js/pdf_viewer.js',
        ],
    },

}